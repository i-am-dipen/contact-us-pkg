<?php

namespace Dipen\Contact\Http\Controllers;

use App\Http\Controllers\Controller;
use Dipen\Contact\Mail\ContactMailable;
use Dipen\Contact\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index(){
        return view('contact::contact');
    }

    public function send(Request $request){
        Mail::to(config('contact.send_email_to'))->send(new ContactMailable($request->name,$request->email,$request->subject,$request->message));
        Contact::create($request->all());
        return redirect(route('contact'));
    }
}