@component('mail::message')
# New query recived from {{ config('app.name') }}

<table class="table">
    <tbody>
        <tr>
            <th scope="col">Name</th>
            <td>{{$name}}</td>
        </tr>
        <tr>
            <th scope="col">Email</th>
            <td>{{$email}}</td>
        </tr>
        <tr>
            <th scope="col">Subject</th>
            <td>{{$subject}}</td>
        </tr>
        <tr>
            <th scope="col">Message</th>
            <td>{{$message}}</td>
        </tr>
    </tbody>
</table>

Thanks,<br>
{{ config('app.name') }}
@endcomponent